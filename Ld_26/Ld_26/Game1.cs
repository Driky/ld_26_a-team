﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

using Ld_26_Lib;
using System.Diagnostics;

namespace Ld_26
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        SpriteManager spriteManager;

        Texture2D studioSplash;
        Texture2D ludumSplash;
        int screenWidth;
        int screenHeight;
        float splashTimer = 2.5f; // seconds for splash

        enum GameState { SplashStudio, SplashLudum, Menu, Play, Pause, GameOver, PlayTest };
        GameState currentGameState = GameState.SplashLudum;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            

            spriteManager = new SpriteManager(this);
            Components.Add(spriteManager);
            spriteManager.Enabled = false;
            spriteManager.Visible = false;
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            screenWidth = GraphicsDevice.Viewport.Width;
            screenHeight = GraphicsDevice.Viewport.Height;
            // Splash screen images
            studioSplash = Content.Load<Texture2D>("ATeamSplash");
            ludumSplash =  Content.Load<Texture2D>("ludumSplash");
            
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {

            float elapsedTime = (float)gameTime.ElapsedGameTime.TotalSeconds;

            switch (currentGameState) 
            { 
                case GameState.SplashStudio:                   
                    splashTimer -= elapsedTime;

                    if (splashTimer <= 0)
                    {   
                       // splashTimer = 2.5f; // Reset timer
                        splashTimer = 0.5f; // Reset timer
                        //currentGameState = GameState.Menu;
                        currentGameState = GameState.PlayTest;
                    }
                    break;
                case GameState.SplashLudum:
                    
                    
                    splashTimer -= elapsedTime;

                    if (splashTimer <= 0)
                    {   
                        //splashTimer = 4.0f; // Reset timer
                        splashTimer = 0.50f; // Reset timer
                        currentGameState = GameState.SplashStudio;
                    }
                    break;
                case GameState.Menu:

                    break;
                case GameState.Play:

                    break;
                case GameState.Pause:

                    break;
                case GameState.GameOver:

                    break;
                case GameState.PlayTest:

                    spriteManager.Enabled = true;
                    spriteManager.Visible = true;

                    break;
            }

            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            // TODO: Add your update logic here

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            Rectangle screenRectangle = new Rectangle(0, 0, screenWidth, screenHeight);

            switch (currentGameState)
            {
                case GameState.SplashStudio:
                    spriteBatch.Begin();
                    spriteBatch.Draw(studioSplash, screenRectangle, Color.White);
                    spriteBatch.End();
                    break;
                case GameState.SplashLudum:
                    spriteBatch.Begin();
                    spriteBatch.Draw(ludumSplash, screenRectangle, Color.White);
                    spriteBatch.End();
                    break;
                case GameState.Menu:
                    break;
                case GameState.Play:
                    break;
                case GameState.Pause:
                    break;
                case GameState.GameOver:
                    break;
            }

            base.Draw(gameTime);
        }

    }
}
