﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Ld_26_Lib
{
    public class Particle
    {
        #region Properties

        public AnimatedSprite ParticleSprite
        {
            get;
            set;
        }

        public float BirthTime
        {
            get;
            set;
        }

        public float MaxAge
        {
            get;
            set;
        }

        public Vector2 OriginalPosition
        {
            get;
            set;
        }

        public Vector2 Acceleration
        {
            get;
            set;
        }

        public Vector2 Direction
        {
            get;
            set;
        }

        public Vector2 Position
        {
            get;
            set;
        }

        public float Scaling
        {
            get;
            set;
        }

        public Color ModColor
        {
            get;
            set;
        }

        #endregion
        
        public Particle(AnimatedSprite particleSprite, float birthTime, float maxAge,
            Vector2 originalPos, Vector2 accel, Vector2 dir, Vector2 pos, float scaling, Color modColor)
        {
            this.ParticleSprite = particleSprite;
            this.BirthTime = birthTime;
            this.MaxAge = maxAge;
            this.OriginalPosition = originalPos;
            this.Acceleration = accel;
            this.Direction = dir;
            this.Position = pos;
            this.Scaling = scaling;
            this.ModColor = modColor;
        }



    }
}
