﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Ld_26_Lib
{
    public class AnimatedSprite : Sprite
    {
        int currentFrame;
        int sheetSize;
        Point frameSize;
        Rectangle rectangl;

        //Speed Stuff
        Vector2 originalSpeed;
        protected Vector2 speed;

        // Framerate stuff
        int timeSinceLastFrame = 0;
        int millisecondsPerFrame;
        const int defaultMillisecondsPerFrame = 500; //16

        public AnimatedSprite(Texture2D laTexture, Vector2 laPosition, List<Rectangle> lesHitsBoxs, Point laframeSize, int laSheetSize)
            : base(laTexture, laPosition, lesHitsBoxs)
        {
            currentFrame = 1;
            sheetSize = laSheetSize;
            frameSize = laframeSize;
            this.scale = 0.25f;
            this.millisecondsPerFrame = defaultMillisecondsPerFrame;
        }

        public override void Update(GameTime gameTime, Rectangle clientBounds)
        {

            // Update frame if time to do so based on framerate
            timeSinceLastFrame += gameTime.ElapsedGameTime.Milliseconds;
            if (timeSinceLastFrame > millisecondsPerFrame)
            {
                // Increment to next frame
                timeSinceLastFrame = 0;
                if (currentFrame == 1)
                {
                    currentFrame = 2;
                }
                else 
                {
                    currentFrame = 1;
                }
            }

            this.rectangl = new Rectangle((currentFrame * frameSize.X ), 0,
                    frameSize.X, frameSize.Y);
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(spriteTexture,
                position,
                rectangl,
                Color.White, 0, Vector2.Zero,
                scale, SpriteEffects.None, 0);
        }


    }
}
