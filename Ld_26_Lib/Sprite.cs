﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Ld_26_Lib
{
    public class Sprite
    {
        protected Texture2D spriteTexture;
        protected Vector2 position;
        protected List<Rectangle> collisionRectList;
        protected float scale = 1;
        protected float originalScale = 1;  

        public Sprite(Texture2D laTexture, Vector2 laPosition, List<Rectangle> lesHitsBoxs) 
        {
            this.spriteTexture = laTexture;
            this.position = laPosition;
            this.collisionRectList = lesHitsBoxs;
        }

        public virtual void Update(GameTime gameTime, Rectangle clientBounds)
        {
        }

        public virtual void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(this.spriteTexture, this.position, Color.White);
        }
    }
}
